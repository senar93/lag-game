﻿/* 
 * composizione stringhe dati
 * "Player" <nomePlayer>
 * "Score " <nomeScena>, <valore>
 * "Unlocked " <nomeScena>, 1 (true) / 0 (false)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(DontDestroyOnLoadScript))]
public class GameManager : MonoBehaviour {
	public static GameManager gameManagerPointer;

	public int targetFrameRate = 30;
	public int vSyncCount = 1;

	public string sceneMainMenu;
	public string sceneInGameMenu;
	public int actScene;

	public List<string> sceneLevel;

	public string playerName;
	public List<bool> unlockedLevel;
	public List<int> scoreLevel;


	#region Scene Manager
	public void SceneLoadLevel(int levelIndex) {
		if (unlockedLevel[levelIndex] && levelIndex < sceneLevel.Count && levelIndex >= 0)
			SceneManager.LoadScene(sceneLevel[levelIndex], LoadSceneMode.Single);
	}

	public void SceneLoadMainMenu() {
		SceneManager.LoadScene(sceneMainMenu, LoadSceneMode.Single);
	}

	public void SceneLoadInGameMenu(bool setTimeScale0 = true) {
		SceneManager.LoadScene(sceneInGameMenu, LoadSceneMode.Additive);
		if(setTimeScale0)
			Time.timeScale = 0;
	}

	#endregion

	#region Save / Load

		#region Save
	public void SaveScore(float score, int levelIndex) {
		if (scoreLevel[levelIndex] < score) {					//salva il punteggio se è maggiore del miglior punteggio precedente
			scoreLevel[levelIndex] = (int) score;
			PlayerPrefs.SetInt("Score " + sceneLevel[levelIndex], (int) score);
			PlayerPrefs.Save();
		}
	}

	public void SaveUnlock(int levelIndex) {
		if (levelIndex < unlockedLevel.Count) {					//sblocca il livello successivo se esiste
			unlockedLevel[levelIndex] = true;
			PlayerPrefs.SetInt("Unlocked " + sceneLevel[levelIndex], 1);
			PlayerPrefs.Save();
		}
	}

	public void SavePlayerName() {
		//Debug.Log(playerName);
		PlayerPrefs.SetString("Player", playerName);
		//Debug.Log(PlayerPrefs.GetString("Player"));
		PlayerPrefs.Save();
	}
		#endregion

		#region Load
	public void LoadScoreLevel() {
		scoreLevel = new List<int>();
		for (int i = 0; i < sceneLevel.Count; i++) {
				scoreLevel.Add(PlayerPrefs.GetInt("Score " + sceneLevel[i]));
		}
	}

	public void LoadUnlockdLevel() {
		unlockedLevel = new List<bool>();
		for (int i = 0; i < sceneLevel.Count; i++) {
			if (PlayerPrefs.GetInt("Unlocked " + sceneLevel[i]) == 1)
				unlockedLevel.Add(true);
			else
				unlockedLevel.Add(false);
		}
	}

	public void LoadPlayerName() {
		playerName = PlayerPrefs.GetString("Player");
	}
		#endregion

	public void DeleteAllSave() {
		PlayerPrefs.DeleteAll();
	}

	#endregion


	public void WinLevel(float score, int levelIndex) {
		SaveScore(score, levelIndex);
		SaveUnlock(levelIndex + 1);
	}



	#region Awake / Start / Update
	// Use this for initialization
	void Awake() {
		GameManager.gameManagerPointer = this;
		QualitySettings.vSyncCount = vSyncCount;
		Application.targetFrameRate = targetFrameRate;

		LoadScoreLevel();
		LoadUnlockdLevel();
		LoadPlayerName();

		SceneLoadMainMenu();
	}

	#endregion

}
