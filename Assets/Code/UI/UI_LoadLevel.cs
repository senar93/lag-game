﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_LoadLevel : MonoBehaviour {

	public int index = -1;
	public Text nameText;
	public Text scoreText;

	public void OnClick_LoadScene() {
		GameManager.gameManagerPointer.SceneLoadLevel(index);
	}


	public void CheckEnable() {
		if (index >= 0 && index < GameManager.gameManagerPointer.sceneLevel.Count && GameManager.gameManagerPointer.unlockedLevel[index])
			this.gameObject.SetActive(true);
		else
			this.gameObject.SetActive(false);
	}
	
}
