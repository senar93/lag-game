﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI_SetName : MonoBehaviour {

	[SerializeField] private GameObject standardUI;

	public void TextBoxEnd_SetName(string newName) {
		GameManager.gameManagerPointer.playerName = newName;
		GameManager.gameManagerPointer.SavePlayerName();
		GameManager.gameManagerPointer.SaveUnlock(0);

		Destroy(GameManager.gameManagerPointer.gameObject);
		SceneManager.LoadScene("Starter", LoadSceneMode.Single);
	}
	

	// Update is called once per frame
	void Awake () {
		if (GameManager.gameManagerPointer.playerName != "") {
			this.gameObject.SetActive(false);
			standardUI.SetActive(true);
		} else {
			this.gameObject.SetActive(true);
			standardUI.SetActive(false);
		}
	}

}
