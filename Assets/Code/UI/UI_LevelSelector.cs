﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_LevelSelector : MonoBehaviour {

	public GameObject prefabButton;
	public Transform levelGrid;

	// Use this for initialization
	void Awake () {
		GameManager pointerGM = GameManager.gameManagerPointer;
		for(int i = 0; i < pointerGM.sceneLevel.Count; i++) {
			GameObject tmp = Instantiate(prefabButton);
			tmp.name = pointerGM.sceneLevel[i];

			tmp.GetComponent<UI_LoadLevel>().index = i;
			tmp.GetComponent<UI_LoadLevel>().CheckEnable();
			tmp.GetComponent<UI_LoadLevel>().nameText.text = tmp.name;
			tmp.GetComponent<UI_LoadLevel>().scoreText.text = pointerGM.scoreLevel[i].ToString();


			tmp.transform.SetParent(levelGrid);
		}
	}

}
