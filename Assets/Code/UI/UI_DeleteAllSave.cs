﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI_DeleteAllSave : MonoBehaviour {

	[SerializeField] private GameObject standardUI;
	[SerializeField] private GameObject newNameUI;

	public void OnClick_DeleteAllSave() {
		GameManager.gameManagerPointer.DeleteAllSave();
		Destroy(GameManager.gameManagerPointer.gameObject);
		SceneManager.LoadScene("Starter", LoadSceneMode.Single);
	}

}
