﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Status : MonoBehaviour {

	public Color c3;
	public Color c2;
	public Color c1;

	public Color timer;
	public Color end;
	
	// Update is called once per frame
	void Update () {
		LevelManager lvlMPointer = LevelManager.levelManagerPointer;
		if (lvlMPointer.startTime.GetActTime() < 1) {
			this.GetComponent<Text>().text = "3";
			this.GetComponent<Text>().color = c3;
		} else if(lvlMPointer.startTime.GetActTime() < 2) {
			this.GetComponent<Text>().text = "2";
			this.GetComponent<Text>().color = c2;
		} else if(!lvlMPointer.startTime.Check()) {
			this.GetComponent<Text>().text = "1";
			this.GetComponent<Text>().color = c1;
		} else if (lvlMPointer.gameEndAllPlayer) {
			this.GetComponent<Text>().text = "END " + lvlMPointer.endTime.GetActTime().ToString("F3");
			this.GetComponent<Text>().color = end;
		} else if (!lvlMPointer.endTime.Check()) {
			this.GetComponent<Text>().text = "GO! - " + lvlMPointer.endTime.GetActTime().ToString("F3");
			this.GetComponent<Text>().color = timer;
		}

	}



}
