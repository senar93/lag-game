﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardInputSource : AbstractInputSource {

	public override float GetSpeedMight() {
		return Input.GetAxis("Vertical");
	}

	public override float GetRotationMight() {
		return Input.GetAxis("Horizontal");
	}

}
