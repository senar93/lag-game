﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractInputSource {

	public abstract float GetSpeedMight();
	public abstract float GetRotationMight();
	
}
