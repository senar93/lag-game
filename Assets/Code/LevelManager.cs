﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {
	public static LevelManager levelManagerPointer;

	#region Level Parameter
	public float maxScore = 1000000f;
	public float badScorePenality = 10f;
	public float lagMight = 5f;
	public float extraTimeAfterEnd = 1f;
	public float startingRotation = 0f;
	public bool soloLevel = true;
	#endregion

	#region Timer / Flag
	/// <summary>
	/// indica se il giocatore ha premuto o meno il pulsante per la partenza
	/// </summary>
	public bool gameStarted = false;
	/// <summary>
	/// timer per la partenza
	/// </summary>
	public Timer startTime = new Timer(5, 0);
	/// <summary>
	/// tempo mancante alla fine della partita
	/// </summary>
	public Timer endTime = new Timer(20, 0);
	/// <summary>
	/// tempo da aspettare dopo la fine della partita prima del termine definitivo
	/// </summary>
	public Timer waitAfterEnd = new Timer(0, 0);
	/// <summary>
	/// indica se è avvenuta o meno la partenza
	/// </summary>
	private bool startTimeExpired = false;
	/// <summary>
	/// indica se tutti i giocatori hanno terminato la partita
	/// </summary>
	public bool gameEndAllPlayer = false;
	#endregion

	#region Global Multiplier
	/// <summary>
	/// scala la velocità massima e minima
	/// </summary>
	public float speedCapMul = 1;
	/// <summary>
	/// scala l'accellerazione
	/// </summary>
	public float accelerationMul = 1;
	/// <summary>
	/// scala la velocità di rotazione
	/// </summary>
	public float rotationSpeedMul = 1;
	#endregion

	/// <summary>
	/// contiene la lista dei GameObject taggati "Player"
	/// </summary>
	public GameObject[] players = null;



	/// <summary>
	/// avvia il termine della partita per il giocatore passato come parametro
	/// </summary>
	/// <param name="win">indica se il giocatore ha terminato o meno il percorso</param>
	/// <param name="player">puntatore al giocatore per cui è stata richiamata la funzione</param>
	public void PlayerGameEnd(bool win, GameObject player) {
		Score tmpScore = player.GetComponent<Score>();
		if (!tmpScore.gameEnd) {											//blocco eseguito solo se il giocatore non ha gia terminato la partita
			tmpScore.gameEnd = true;
			tmpScore.enabled = false;
			player.GetComponent<PlanarMovement>().enabled = false;

			gameEndAllPlayer = true;
			foreach (GameObject tmpPlayer in players)							//verifica se tutti i giocatori hanno terminato la partita
				if (!tmpPlayer.GetComponent<Score>().gameEnd)
					gameEndAllPlayer = false;

			if (soloLevel) {
				if (win)
					GameManager.gameManagerPointer.WinLevel(player.GetComponent<Score>().actScore, GameManager.gameManagerPointer.actScene);
			}
			//DA IMPLEMENTARE (else...)
			//comportamento in caso la modalità sia un qualche tipo di multiplayer
		}
	}

	/// <summary>
	/// richiamata al termine della partita, dopo che tutti i giocatori hanno terminato il movimento ed è stato visualizzato assieme alla lag
	/// </summary>
	public void GameEnd() {
		if (soloLevel)
			GameManager.gameManagerPointer.SceneLoadMainMenu();
		//DA IMPLEMENTARE (else...)
		//comportamento in caso la modalità sia un qualche tipo di multiplayer
	}

	/// <summary>
	/// calcola il punteggio del giocatore passato come parametro nel frame attuale
	/// </summary>
	/// <param name="pScore">giocatore di cui calcolare il punteggio</param>
	public void CalcScore(Score pScore) {
		pScore.actScore = 1 + maxScore / (pScore.timeGood.GetActTime() + pScore.timeBad.GetActTime() * badScorePenality);
	}



	#region Awake / Start / Update
	// Use this for initialization
	void Awake () {
		levelManagerPointer = this;

		waitAfterEnd = new Timer(lagMight*2 + extraTimeAfterEnd, 0);

		#region PLAYER initialize

		players = GameObject.FindGameObjectsWithTag("Player");
		GameObject startPoint = GameObject.FindGameObjectWithTag("StartPoint");
		int nameIndex = 0;													//contatore sommato al nome dei giocatori per renderli univoci
		foreach(GameObject player in players) {
			PlanarMovement tmp = player.GetComponent<PlanarMovement>();

			if (soloLevel) {
				player.name = GameManager.gameManagerPointer.playerName;
				tmp.ghost.name = player.name + " - Ghost";
			} else {
				player.name += " - " + nameIndex++;
				tmp.ghost.name = player.name + " - Ghost";
			}

			if (startPoint != null) {											//il blocco viene eseguito solo se esiste un oggetto con tag StartPoint 
				//imposta la posizione del giocatore
				player.transform.position = startPoint.transform.position;
				tmp.rotation.rotation = startingRotation;
				player.transform.eulerAngles = new Vector3(0, 0, startingRotation);
				//imposta la posizione del fantasma
				tmp.ghost.transform.position = startPoint.transform.position;
				tmp.ghost.transform.rotation = player.transform.rotation;
			}
			tmp.speed.speedMax *= speedCapMul;
			tmp.speed.speedMin *= speedCapMul;
			tmp.speed.acceleration *= accelerationMul;
			tmp.rotation.rotationSpeed *= rotationSpeedMul;
			tmp.enabled = false;
			player.GetComponent<Score>().enabled = false;
		}

		#endregion

	}


	// Update is called once per frame
	void Update () {
		if (gameStarted) {
			#region NOT startTimeExpired
			if (!startTimeExpired)
			{                                       //blocco eseguito in tutti i frame prima del termine del timer dello start
				startTime.Advance();
				if (startTime.Check())
				{                                       //blocco eseguito solo nel primo frame in cui scade il timer dello start
					startTimeExpired = true;
					foreach (GameObject player in players)
					{                       //abilita i giocatori alla partenza
						player.GetComponent<PlanarMovement>().enabled = true;
						player.GetComponent<Score>().enabled = true;
					}
				}
			}
			#endregion

			#region startTimeExpired AND NOT gameEndAllPlayer
			if (startTimeExpired && !gameEndAllPlayer)
			{                   //blocco eseguito in tutti i frame dopo il termine del timer dello start prima della fine della partita
				foreach (GameObject player in players)
				{                        //calcola il punteggio del giocatore frame per frame
					CalcScore(player.GetComponent<Score>());
				}
				endTime.Advance();
				if (endTime.Check())
				{                                           //blocco eseguito solo nel primo frame in cui scade il timer della partita
					foreach (GameObject player in players)
					{                           //richiama il termine della partita con sconfitta per tutti i giocatori che non la hanno ancora terminata
						PlayerGameEnd(false, player);
					}
				}
			}
			#endregion

			#region gameEndAllPlayer AND  NOT waitAfterEnd.Check()
			if (gameEndAllPlayer && !waitAfterEnd.Check())
			{               //blocco eseguito in tutti i frame dopo il termine della partita prima del termine del tempo supplementare
				waitAfterEnd.Advance();
				if (waitAfterEnd.Check())                                   //eseguito solo una volta al termine del tempo supplementare 
					GameEnd();
			}
			#endregion
		}

	}

	#endregion


}
