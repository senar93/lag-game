﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

public class DEBUG_XmlLoader : MonoBehaviour {

	public PlanarMovement player;

	public bool save = true;
	public bool load = true;

	public SAVE store = new SAVE();



	// Use this for initialization
	void Start () {
		if (player == null)
			player = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<PlanarMovement>();

		if (load) {
			store.Load();
			player.speed = store.speed;
			player.rotation.rotationSpeed = store.rotationSpeed;
		}

		if (save) {
			store.speed = player.speed;
			store.rotationSpeed = player.rotation.rotationSpeed;
			store.Save();
		}

		Destroy(this);
	}

}

[System.Serializable]
public class SAVE {

	public Speed speed;
	public float rotationSpeed;

	public void Save() {
		var serializer = new XmlSerializer(typeof(SAVE));
		var stream = new FileStream(Path.Combine(Application.dataPath, "DEBUG.xml"), FileMode.Create);
		serializer.Serialize(stream, this);
		stream.Close();
	}

	public void Load() {
		var serializer = new XmlSerializer(typeof(SAVE));
		var stream = new FileStream(Path.Combine(Application.dataPath, "DEBUG.xml"), FileMode.Open);
		SAVE tmp = serializer.Deserialize(stream) as SAVE;

		speed = tmp.speed;
		rotationSpeed = tmp.rotationSpeed;

		stream.Close();
	}

}
