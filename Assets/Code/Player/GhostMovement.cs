﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostMovement : MonoBehaviour {

	public GameObject player;
	public Timer playerTimer = new Timer(3600, 0);
	//public Timer ghostTimer = new Timer(3600, 0);

	public List<GhostData> ghostDataList;

	// Use this for initialization
	void Start () {
		ghostDataList = new List<GhostData>();
	}
	
	// Update is called once per frame
	void Update () {
		LevelManager lvlMPointer = LevelManager.levelManagerPointer;
		if (lvlMPointer.startTime.Check() && !lvlMPointer.gameEndAllPlayer)
		{
			playerTimer.Advance();
			ghostDataList.Add( new GhostData(player.transform.position, player.transform.rotation, playerTimer.GetActTime()) );
		}

		float lagTime = lvlMPointer.endTime.GetActTime() + lvlMPointer.waitAfterEnd.GetActTime() - lvlMPointer.lagMight;

		if (lagTime >= 0) {
			if (ghostDataList != null) {
				int i;
				for (i = 0; i < ghostDataList.Count; i++) {
					if(ghostDataList[i].timing > lagTime) {
						break;
					}
				}

				if (i >= ghostDataList.Count)
					i = ghostDataList.Count - 1;

				transform.position = ghostDataList[i].position;
				transform.rotation = ghostDataList[i].rotation;

				if (i < ghostDataList.Count - 1)
					ghostDataList.RemoveRange(0, i);

			}
		}
		
	}


}
