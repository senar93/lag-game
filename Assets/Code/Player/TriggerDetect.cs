﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class TriggerDetect : MonoBehaviour {

	public bool goodTriggerFlag = false;
	public bool badTriggerFlag = false;
	public bool endTriggerFlag = false;


	#region OnTrigger
	void OnTriggerEnter2D(Collider2D other) {
		if(other.tag == "EndTrack") {
			endTriggerFlag = true;
			LevelManager.levelManagerPointer.PlayerGameEnd(true, this.gameObject);
		}
		if(other.tag == "GoodField") {
			goodTriggerFlag = true;
		}
		if(other.tag == "BadField") {
			badTriggerFlag = true;
		}

	}

	void OnTriggerExit2D(Collider2D other) {
		/*if (other.tag == "EndTrack") {
			endTriggerFlag = false;
		}*/
		if (other.tag == "GoodField") {
			goodTriggerFlag = false;
		}
		if (other.tag == "BadField") {
			badTriggerFlag = false;
			LevelManager.levelManagerPointer.PlayerGameEnd(false, this.gameObject);
		}

	}

	#endregion

}
