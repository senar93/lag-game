﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlanarMovement : MonoBehaviour {
	public GhostMovement ghost;
	public Speed speed = new Speed();
	public Rotation rotation = new Rotation(true);
	[HideInInspector] public AbstractInputSource inputSurce;



	#region Awake / Start / Update
	// Use this for initialization
	void Start () {
		inputSurce = new StandardInputSource();
	}
	
	// Update is called once per frame
	void Update () {
		speed.Update(inputSurce.GetSpeedMight());
		rotation.Update(inputSurce.GetRotationMight());
		transform.eulerAngles = new Vector3(0, 0, rotation.rotation);
		transform.position = new Vector2(transform.position.x + (speed.speed * Mathf.Cos(rotation.rotation * Mathf.PI / 180)),
										 transform.position.y + (speed.speed * Mathf.Sin(rotation.rotation * Mathf.PI / 180)) );
	}

	#endregion

}
