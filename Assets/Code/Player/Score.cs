﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TriggerDetect))]
public class Score : MonoBehaviour {

	public float actScore = 0;
	public bool gameEnd = false;
	public Timer timeGood = new Timer(3600, 0);
	public Timer timeBad = new Timer(3600, 0);
	private TriggerDetect trigger;



	#region Awake / Start / Update
	// Use this for initialization
	void Start () {
		trigger = this.GetComponent<TriggerDetect>();
	}
	
	// Update is called once per frame
	void Update () {
		if(!trigger.endTriggerFlag)	
			if (trigger.goodTriggerFlag)
				timeGood.Advance();
			else if(trigger.badTriggerFlag)
				timeBad.Advance();
	}

	#endregion

}
