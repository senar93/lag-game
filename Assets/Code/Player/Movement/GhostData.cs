﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable] public class GhostData {
	public Vector3 position;
	public Quaternion rotation;
	public float timing;

	public GhostData(Vector3 p, Quaternion r, float t) {
		this.position = p;
		rotation = r;
		timing = t;
	}


}