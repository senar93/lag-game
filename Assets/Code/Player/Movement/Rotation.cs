﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// classe che rappresenta la rotazione del giocatore
/// </summary>
[System.Serializable] public class Rotation {

	/// <summary>
	/// rotazione grezza del oggetto, usata solo in get e set di rotation
	/// </summary>
	[SerializeField] private float _rotation = 0;
	/// <summary>
	/// velocità di rotazione grezza del oggetto, usata solo in get e set di rotationSpeed
	/// </summary>
	[SerializeField] private float _rotationSpeed = 0;
	/*/// <summary>
	/// velocità di rotazione massima del oggetto
	/// </summary>
	//public float rotationSpeedMax = 270;*/

	/// <summary>
	/// viene moltiplicato al valore dell asse preso in input per la rotazione, invertendone il significato
	/// </summary>
	private int _reverseAxes;
	


	/// <summary>
	/// rotazione attuale del oggetto
	/// </summary>
	[HideInInspector] public float rotation {
		get {
			return _rotation;
		}
		set {
			if (value > 180)
				_rotation = -value;
			else if (value < - 180)
				_rotation = -value;
			else
				_rotation = value;
		}
	}

	/// <summary>
	/// velocità di rotazione attuale del oggetto
	/// </summary>
	[HideInInspector] public float rotationSpeed {
		get {
			return _rotationSpeed;
		}
		set {
			/*if (value > rotationSpeedMax)
				_rotationSpeed = rotationSpeedMax;
			else*/
			_rotationSpeed = value;
		}
	}

	/// <summary>
	/// inverte l'input
	/// </summary>
	[HideInInspector] public bool reverseAxes {
		get {
			return (_reverseAxes < 0) ? true : false;
		}
		set {
			_reverseAxes = (value == true) ? -1 : 1;
		}
	}



	/// <summary>
	/// costruttore di Rotation
	/// </summary>
	/// <param name="reverseAxes">inverte il significato dell input</param>
	public Rotation(bool reverseAxes = true) {
		this.reverseAxes = reverseAxes;
	}



	/// <summary>
	/// aggiorna rotation al valore corretto per l'fps in cui si trova
	/// </summary>
	/// <param name="inputMight">input sul asse orizzontale</param>
	public void Update(float inputMight) {
		rotation += rotationSpeed * inputMight * Time.deltaTime * _reverseAxes;
	}

}
