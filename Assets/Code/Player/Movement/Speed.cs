﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// classe che rappresenta la velocità del giocatore
/// </summary>
[System.Serializable] public class Speed {
	
	/// <summary>
	/// velocità grezza del oggetto, usata solo in get e set di speed
	/// </summary>
	[SerializeField] private float _speed = 0;
	/// <summary>
	/// velocità massima raggiungibile
	/// </summary>
	public float speedMax = 5;
	/// <summary>
	/// velocità minima raggiungibile
	/// </summary>
	public float speedMin = 0;
	/// <summary>
	/// accellerazione massima
	/// </summary>
	public float acceleration = 1;
	/// <summary>
	/// decellerazione massima
	/// </summary>
	public float decellaration = 10;
	/// <summary>
	/// valore sottratto al input se l'input è <= 0
	/// </summary>
	public float zeroInputThreshold = 0.1f;


	/// <summary>
	/// velocità attuale del oggetto
	/// </summary>
	[HideInInspector] public float speed {
		get {
			return _speed;
		}
		set {
			if (value > speedMax)
				_speed = speedMax;
			else if (value < speedMin)
				_speed = speedMin;
			else
				_speed = value;
		}
	}



	/// <summary>
	/// aggiorna speed al valore corretto per l'fps in cui si trova
	/// </summary>
	/// <param name="inputMight">input sul asse verticale</param>
	public void Update(float inputMight) {
		if (inputMight < zeroInputThreshold && inputMight > -zeroInputThreshold)
			speed = 0;
		else if (inputMight > 0)
			speed += acceleration * inputMight * Time.deltaTime;
		else
			speed += decellaration * inputMight * Time.deltaTime;
	}

}
